namespace PassengerTraffic
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Tickets
    {
        [Key]
        public int Id { get; set; }

        public int IdTypeTicket { get; set; }

        [NotMapped]
        public string TypeTicketStr => TypesTickets.NameType;

        public decimal Price { get; set; }

        public int IdDirection { get; set; }

        public virtual Directions Directions { get; set; }

        public virtual TypesTickets TypesTickets { get; set; }
    }
}