namespace PassengerTraffic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Cities
    {
        public Cities()
        {
            Directions = new HashSet<Directions>();
            Directions1 = new HashSet<Directions>();
        }

        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public int IdCountry { get; set; }

        public virtual Countries Countries { get; set; }

        public virtual ICollection<Directions> Directions { get; set; }

        public virtual ICollection<Directions> Directions1 { get; set; }
    }
}
