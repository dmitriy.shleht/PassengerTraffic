namespace PassengerTraffic
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Directions
    {
        public Directions()
        {
            Tickets = new HashSet<Tickets>();
        }

        [Key]
        public int Id { get; set; }

        public int Number�ruise { get; set; }

        public DateTime DateDeparture { get; set; }

        [NotMapped]
        public string DateDepartureStr => DateDeparture.ToString("dd.MM.yyyy HH:mm");

        public DateTime DateArrival { get; set; }

        [NotMapped]
        public string DateArrivalStr => DateArrival.ToString("dd.MM.yyyy HH:mm");

        public int IdTransport { get; set; }

        [NotMapped]
        public string TransportStr
        {
            get
            {
                if (Transports != null)
                    return Transports.NameAuto + ", " + Transports.GosNumber;

                return "";
            }
        }

        public int Departure { get; set; }

        [NotMapped]
        public string DepartureStr
        {
            get
            {
                if (Cities != null)
                    return Cities.Countries.Name + ", " + Cities.Name;

                return "";
            }
        }

        public int Arrival { get; set; }

        [NotMapped]
        public string ArrivalStr
        {
            get
            {
                if (Cities1 != null)
                    return Cities1.Countries.Name + ", " + Cities1.Name;

                return "";
            }
        }

        public int CountAvailable { get; set; }

        public virtual Cities Cities { get; set; }

        public virtual Cities Cities1 { get; set; }

        public virtual Transports Transports { get; set; }

        public virtual ICollection<Tickets> Tickets { get; set; }
    }
}
