namespace PassengerTraffic
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class SellTickets
    {
        [Key]
        public int Id { get; set; }

        public int Number�ruise { get; set; }

        public DateTime DateSale { get; set; }

        public decimal Summ { get; set; }

        public string TypeTicket { get; set; }

        public string DepartureCountry { get; set; }

        public string DepartureCity { get; set; }

        public string ArrivalCountry { get; set; }

        public string ArrivalCity { get; set; }

        public DateTime DateDeparture { get; set; }

        public DateTime DateArrival { get; set; }

        public string Transport { get; set; }

        public string GosNumber { get; set; }
    }
}
