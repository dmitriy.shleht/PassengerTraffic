﻿using System;
using System.Windows.Forms;

namespace PassengerTraffic
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Перехват всех необработанных исключений
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += (sender, e) => 
            {
                Exception ex = e.Exception;
                // Идем по вниз по стеку внутренних исключений и пробрасываем самое последнее
                while (ex.InnerException != null)
                    ex = ex.InnerException;

                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            };

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
