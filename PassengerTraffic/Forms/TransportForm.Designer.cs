﻿namespace PassengerTraffic.Forms
{
    partial class TransportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridViewTransports = new System.Windows.Forms.DataGridView();
            this.NameAuto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GosNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberOfSeats = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTransports)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewTransports
            // 
            this.gridViewTransports.AllowUserToOrderColumns = true;
            this.gridViewTransports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewTransports.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameAuto,
            this.GosNumber,
            this.NumberOfSeats});
            this.gridViewTransports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridViewTransports.Location = new System.Drawing.Point(0, 0);
            this.gridViewTransports.Name = "gridViewTransports";
            this.gridViewTransports.Size = new System.Drawing.Size(544, 241);
            this.gridViewTransports.TabIndex = 0;
            // 
            // NameAuto
            // 
            this.NameAuto.DataPropertyName = "NameAuto";
            this.NameAuto.HeaderText = "Наименование";
            this.NameAuto.Name = "NameAuto";
            this.NameAuto.Width = 300;
            // 
            // GosNumber
            // 
            this.GosNumber.DataPropertyName = "GosNumber";
            this.GosNumber.HeaderText = "Гос номер";
            this.GosNumber.Name = "GosNumber";
            // 
            // NumberOfSeats
            // 
            this.NumberOfSeats.DataPropertyName = "NumberOfSeats";
            this.NumberOfSeats.HeaderText = "Кол-во мест";
            this.NumberOfSeats.Name = "NumberOfSeats";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSave.Location = new System.Drawing.Point(0, 218);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(544, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TransportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 241);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gridViewTransports);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TransportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Транспорт";
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTransports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridViewTransports;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameAuto;
        private System.Windows.Forms.DataGridViewTextBoxColumn GosNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfSeats;
        private System.Windows.Forms.Button btnSave;
    }
}