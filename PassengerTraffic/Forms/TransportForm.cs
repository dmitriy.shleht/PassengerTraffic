﻿using System;
using System.Data.Entity;
using System.Windows.Forms;

namespace PassengerTraffic.Forms
{
    public partial class TransportForm : Form
    {
        private readonly TrafficContext ctx = new TrafficContext();
        private readonly BindingSource bs = new BindingSource();

        public TransportForm()
        {
            InitializeComponent();

            gridViewTransports.AutoGenerateColumns = false;

            ctx.Transports.Load();
            bs.DataSource = ctx.Transports.Local.ToBindingList();
            gridViewTransports.DataSource = bs;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            gridViewTransports.EndEdit();
            bs.EndEdit();
            ctx.SaveChanges();
            Close();
        }
    }
}
