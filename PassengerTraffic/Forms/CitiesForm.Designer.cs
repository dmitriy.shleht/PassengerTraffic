﻿namespace PassengerTraffic.Forms
{
    partial class CitiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridViewCountries = new System.Windows.Forms.DataGridView();
            this.NameCoutry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridViewCities = new System.Windows.Forms.DataGridView();
            this.NameCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCountries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridViewCountries
            // 
            this.gridViewCountries.AllowUserToOrderColumns = true;
            this.gridViewCountries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewCountries.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameCoutry,
            this.Id});
            this.gridViewCountries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridViewCountries.Location = new System.Drawing.Point(0, 0);
            this.gridViewCountries.Name = "gridViewCountries";
            this.gridViewCountries.Size = new System.Drawing.Size(345, 231);
            this.gridViewCountries.TabIndex = 0;
            this.gridViewCountries.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridView_CellValueChanged);
            // 
            // NameCoutry
            // 
            this.NameCoutry.DataPropertyName = "Name";
            this.NameCoutry.HeaderText = "Наименование страны";
            this.NameCoutry.Name = "NameCoutry";
            this.NameCoutry.Width = 300;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // gridViewCities
            // 
            this.gridViewCities.AllowUserToOrderColumns = true;
            this.gridViewCities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewCities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameCity,
            this.IdCountry});
            this.gridViewCities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridViewCities.Location = new System.Drawing.Point(0, 0);
            this.gridViewCities.Name = "gridViewCities";
            this.gridViewCities.Size = new System.Drawing.Size(346, 231);
            this.gridViewCities.TabIndex = 1;
            this.gridViewCities.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridView_CellValueChanged);
            this.gridViewCities.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.gridViewCities_DefaultValuesNeeded);
            // 
            // NameCity
            // 
            this.NameCity.DataPropertyName = "Name";
            this.NameCity.HeaderText = "Наименование города";
            this.NameCity.Name = "NameCity";
            this.NameCity.Width = 300;
            // 
            // IdCountry
            // 
            this.IdCountry.DataPropertyName = "IdCountry";
            this.IdCountry.HeaderText = "IdCountry";
            this.IdCountry.Name = "IdCountry";
            this.IdCountry.Visible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridViewCountries);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gridViewCities);
            this.splitContainer1.Size = new System.Drawing.Size(695, 231);
            this.splitContainer1.SplitterDistance = 345;
            this.splitContainer1.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSave.Location = new System.Drawing.Point(0, 208);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(695, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // CitiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 231);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.splitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CitiesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Страны - Города";
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCountries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCities)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridViewCountries;
        private System.Windows.Forms.DataGridView gridViewCities;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCoutry;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCountry;
    }
}